//
// Copyright (c) 2008, 2009 Boris Schaeling <boris@highscore.de>
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
#pragma once

#include <boost/locale.hpp>

#include <filesystem>
#include <thread>
#include <fstream>

#define TEST_DIR1 "A95A7AE9-D5F5-459a-AB8D-28649FB1F3F4"
#define TEST_DIR2 "EA63DF88-7BFF-4038-B317-F37434DF4ED1"
#define TEST_FILE1 "test1.txt"
#define TEST_FILE2 "test2.txt"

const std::filesystem::path initial_path = std::filesystem::current_path();

class directory
{
public:
    directory(const char *name)
#ifdef _WIN32
        : full_path(std::filesystem::current_path() / boost::locale::conv::utf_to_utf<wchar_t>(name))
#else
        : full_path(std::filesystem::current_path() / name)
#endif
    {
        std::filesystem::create_directories(full_path);
        BOOST_REQUIRE(std::filesystem::is_directory(full_path));
    }

    ~directory()
    {
        bool again;
        do
        {
            try
            {
                std::filesystem::remove_all(full_path);
                again = false;
            }
            catch (...)
            {
                std::this_thread::yield();
                again = true;
            }
        } while (again);
    }

    std::filesystem::path create_file(const char *file)
    {
        std::filesystem::current_path(full_path);
        BOOST_REQUIRE(std::filesystem::equivalent(full_path, std::filesystem::current_path()));

#ifdef _WIN32
        std::filesystem::path file_path(boost::locale::conv::utf_to_utf<wchar_t>(file));
#else
        std::filesystem::path file_path(file);
#endif
        std::ofstream ofs(file_path);
        BOOST_REQUIRE(std::filesystem::exists(file_path));
        std::filesystem::current_path(initial_path);
        return full_path / file_path;
    }

    std::filesystem::path rename_file(const char *from, const char *to)
    {
        std::filesystem::current_path(full_path);
        BOOST_REQUIRE(std::filesystem::equivalent(full_path, std::filesystem::current_path()));
        BOOST_REQUIRE(std::filesystem::exists(from));
        std::filesystem::rename(from, to);
        BOOST_REQUIRE(std::filesystem::exists(to));
        std::filesystem::current_path(initial_path);
        BOOST_REQUIRE(std::filesystem::equivalent(std::filesystem::current_path(), initial_path));
        return full_path / to;
    }

    void remove_file(const char *file)
    {
        std::filesystem::current_path(full_path);
        BOOST_REQUIRE(std::filesystem::equivalent(full_path, std::filesystem::current_path()));
        BOOST_REQUIRE(std::filesystem::exists(file));
        std::filesystem::remove(file);
        std::filesystem::current_path(initial_path);
        BOOST_REQUIRE(std::filesystem::equivalent(std::filesystem::current_path(), initial_path));
    }

    void write_file(const char *file, const char *buffer)
    {
        std::filesystem::current_path(full_path);
        BOOST_REQUIRE(std::filesystem::equivalent(full_path, std::filesystem::current_path()));
        BOOST_REQUIRE(std::filesystem::exists(file));
        std::ofstream ofs(file);
        BOOST_CHECK(ofs);
        ofs << buffer;
        ofs.close();
        std::filesystem::current_path(initial_path);
        BOOST_REQUIRE(std::filesystem::equivalent(std::filesystem::current_path(), initial_path));
    }

private:
    std::filesystem::path full_path;
};

